# Libertine Tweak Tool

Tweak and tune your desktop apps installed in Libertine

[Ubuntu Touch](https://ubuntu-touch.io/) comes with the [Libertine](http://docs.ubports.com/en/latest/userguide/dailyuse/libertine.html) tool to run
desktop applications on your mobile device. Libertine Tweak Tool allows you to tweak some of these desktop applications to work better on a small touch screen.

## Development

```
git clone https://gitlab.com/doniks/libertine-tweak-tool
cd libertine-tweak-tool
clickable --dirty
clickable logs
```

### Arm64
To build arm64 version, do
```
clickable --arch arm64
```

To some degree it is possible to develop/test with `clickable desktop`. For this, note that clickable sets the home dir to:
`~/.clickable/home/`. So you might want to prepare this directory:
```
mkdir -p ~/.clickable/home/.local/share/libertine-container/user-data/xenial
```

Then you can read/write the files there:
```
cd ~/.clickable/home/.local/share/libertine-container/user-data/xenial
cat .Xdefaults
```

## IDE
You can run `clickable ide qtcreator`.

You can also quickly test qml changes without having to build the whole click everytime:
```
clickable ide bash
QML2_IMPORT_PATH=build/x86_64-linux-gnu/app/install/lib/x86_64-linux-gnu qmlscene qml/Main.qml
```



## Feedback/Issues/Feature requests

Happy to hear your feedback or the tweaks that you apply. Find me in gitlab [issue tracker](https://gitlab.com/doniks/libertine-tweak-tool/issues), or on telegram: dohniks.
