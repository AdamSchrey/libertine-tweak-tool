# Title

Libertine Tweak Tool

# Tag Line

Tweak and tune your Libertine containers and the Desktop apps inside

# Description

The Libertine Tweak Tool allows you to adjust the appearance of desktop apps that you have installed in your Libertine container. You can:

* set the general scaling (dpi)
* add bigger scrollbar in Gtk applications
* configure Transmission torrent appliction
* Libertine Shell to enter the container on the commandline

Permissions:
The app has
* write access to the home directories of your libertine containers /home/phablet/.local/share/libertine-container/user-data/ to apply the tweaks to the applications.
* write access to your .bashrc to extend the PATH variable

If you know about some tweak that improves the usability of some application in Libertine, I gladly hear about it in the issue tracker on gitlab or find me as @dohniks on telegram.


# Category

Utilities

# Keywords

Libertine, X, Xmir, XTerm, Desktop, Convergence, DPI, Transmission, PATH, lish, lirsh
