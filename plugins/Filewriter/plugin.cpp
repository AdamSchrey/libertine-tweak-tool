#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "filewriter.h"

void FilewriterPlugin::registerTypes(const char *uri) {
    //@uri Filewriter
    qmlRegisterSingletonType<Filewriter>(uri, 1, 0, "Filewriter", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Filewriter; });
}
