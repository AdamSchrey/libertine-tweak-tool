#include <QDebug>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QProcess>
#include "filewriter.h"
#include <QJsonDocument>
#include <QJsonObject>

Filewriter::Filewriter():
    libertineRootDir("/home/phablet/.cache/libertine-container/"),
    libertineUserDir("/home/phablet/.local/share/libertine-container/user-data/"),
    //libertineUserDir("/home/phablet/.local/share/libertine-tweak-tool.doniks/"),
    xdefaults(".Xdefaults"),
    gtkcssdir(".config/gtk-3.0"),
    gtkfilename("gtk.css")
{

}

QStringList Filewriter::listContainers(){
    QStringList list;
    list << "xenial";
    // FIXME
}

QString Filewriter::filenameXdefaults(QString container){
    return libertineUserDir + container + "/" + xdefaults;
}

int Filewriter::getXdpi(QString container){
    qDebug() << __FUNCTION__;
    QString filename = filenameXdefaults(container);
    int dpi = -1;

    QFile file(filename);
    if (file.exists()){
        QByteArray fileData;
        if (!file.open(QIODevice::ReadWrite)){
            qCritical() << "Failed to open file" << filename << "[" << file.error() << "]";
            return -1;
        }

        while (!file.atEnd()){
            QString line = file.readLine();
            if (line.isEmpty()){
                continue;
            } else if (QRegExp("^ *#").indexIn(line) != -1 ){
                // comment
                continue;
            } else {
                QString dpis = line.replace(QRegExp("^ *Xft.dpi: *"), "");
                if (!dpis.isEmpty()){
                    dpi = dpis.toInt();
                    qDebug() << "dpis:" << dpis << dpi;
                    break;
                }
            }
        }
        file.close();
    } else {
        qDebug() << "File does not exist" << filename;
    }
    return dpi;
}

void Filewriter::setXdpi(QString container, bool clear, uint dpi){
    qDebug() << __FUNCTION__ << container << clear << dpi;
    bool written = false;

    QString dirname = libertineUserDir + container;
    QDir dir(dirname);
    if ( !dir.exists() ){
        qDebug() << "Dir not found (no such container?)" << dirname;
        return;
    }

    QString filename = filenameXdefaults(container);
    QFile file(filename);
    if (file.exists()){
        // change setting in file
        qDebug() << "change";
        if (!file.open(QIODevice::ReadWrite)){
            qCritical() << "Failed to open file" << filename << "[" << file.error() << "]";
            return;
        }
        QByteArray fileData;

        while (!file.atEnd()){
            QString line = file.readLine();
            if (line.isEmpty()){
                // continue;
            } else if (QRegExp("^ *#").indexIn(line) != -1 ){
                // comment
                // continue;
            } else {
                int index = line.indexOf(QRegExp("^ *Xft.dpi: *"));
                if (index != -1){
                    qDebug() << "before" << "[" << line << "]";
                    line.clear();
                    if (!clear && !written){
                        QTextStream afters(&line, QIODevice::ReadWrite);
                        afters << "Xft.dpi:" << dpi << endl;
                        written = true;
                    }
                    qDebug() << "after" << "[" << line << "]";
                }
            }
            // qDebug() << "{" << fileData << "}" << "+[" << line << "]";
            fileData.append(line);
        }


        // go back to the start
        //if (!file.seek(0)){
        file.close();
        if (!file.open(QIODevice::ReadWrite | QIODevice::Truncate) ){
            qCritical() << "Failed to reopen file" << filename << "[" << file.error() << "]";
            return;
        }

        //qDebug() << "FINAL:{" << fileData << "}";
        if (file.write(fileData) == -1){
            qCritical() << "Failed to write to file" << filename << "[" << file.error() << "]";
            return;
        }
        file.close();
        qDebug() << "change [done]";
    }


    if (!clear){
        // we should write
        if (!file.exists() || !written){
            // but we haven't written yet
            qDebug() << "append";
            if ( file.open(QIODevice::ReadWrite | QIODevice::Append) ){
                QTextStream stream( &file );
                stream << "Xft.dpi: " << dpi << endl;
                file.close();
            }else{
                qCritical() << "Failed to append to file" << filename << "[" << file.error() << "]" ;
            }
        }
    }

    qDebug() << __FUNCTION__ << "[done]";
}

void Filewriter::setXdpi(QString container, uint dpi){
    setXdpi(container, false, dpi);
}

void Filewriter::clearXdpi(QString container){
    setXdpi(container, true, -1);
}














QString Filewriter::filenameGtkCSS(QString container){
    return libertineUserDir + container + "/" + gtkcssdir + "/" + gtkfilename;
}

bool Filewriter::getGtkScroolbar(QString container){
    qDebug() << __FUNCTION__;
    QString dirname = libertineUserDir + container;
    QDir dir(dirname);
    if ( !dir.exists() ){
        qDebug() << "Dir not found (no such container?)" << dirname;
        return false;
    }


    QString filename = filenameGtkCSS(container);
    bool enabled = false;

    QFile file(filename);
    if (file.exists()){
        QByteArray fileData;
        file.open(QIODevice::ReadWrite);
        while (!file.atEnd()){
            QString line = file.readLine();
            if (line.isEmpty()){
                continue;
            } else if (QRegExp("^ *#").indexIn(line) != -1 ){
                // comment
                continue;
            } else if (QRegExp("^.scrollbar").indexIn(line) != -1 ) {
                enabled = true;
                break;
            }
        }
        file.close();
    }
    return enabled;
}

void Filewriter::setGtkScroolbar(QString container){
    qDebug() << __FUNCTION__;
    QString dirname = libertineUserDir + container;
    QDir dir(dirname);
    if ( !dir.exists() ){
        qDebug() << "Dir not found (no such container?)" << dirname;
        return;
    }


    dirname = libertineUserDir + container + "/" + gtkcssdir;
    dir = dirname;
    if (!dir.exists()){
        if ( !dir.mkpath(dirname))
            qCritical() << "Failed to create dir" << dirname;
        else
            qDebug() << "Created" << dirname;
    }

    QString filename = filenameGtkCSS(container);
    QFile file(filename);
    if ( file.open(QIODevice::ReadWrite ) ){
        qDebug() << "writing" << filename;
        QTextStream stream( &file );
        stream << "\
.scrollbar {\
    /* up/down buttons on top */\
    -GtkScrollbar-has-backward-stepper: true; \
    -GtkScrollbar-has-secondary-forward-stepper: true; \
\
    -GtkRange-slider-width: 50; /* width of the scrollbar */ \
    -GtkRange-stepper-size: 50; /* size of up/down buttons */ \
} \
"
        << endl;
        file.close();
    }else{
        qWarning() << "Failed to create file" << filename << "[" << file.error() << "]";
    }
}

void Filewriter::clearGtkScroolbar(QString container){
    qDebug() << __FUNCTION__;
    QString filename = filenameGtkCSS(container);
    QFile file(filename);
    if ( file.remove() )
        qDebug() << "Removed " << filename;
    else
        qCritical() << "Failed to remove" << filename;
}

















void Filewriter::speak() {
    qDebug() << "hello world!"
        << configPath()
        << cachePath()
        << appDataPath();

    auto now = QDateTime::currentDateTime();

    QDir dir(appDataPath());
    if (!dir.exists())
        dir.mkpath(".");

    QString filename=appDataPath() + "/" + "Data.txt";
    QFile file( filename );
    if ( file.open(QIODevice::ReadWrite) ){
        QTextStream stream( &file );
        stream << "appdata "
            << now.toString() << endl;
    }else {
        qWarning() << "failed to write to" << filename << "[" << file.error() << "]";
    }

    filename="/home/phablet/HData.txt";
    file.close();
    file.setFileName(filename);
    if ( file.open(QIODevice::ReadWrite) ){
        QTextStream stream( &file );
        stream << "home "
            << now.toString() << endl;


    }else {
        qWarning() << "failed to write to" << filename << "[" << file.error() << "]";
    }

    filename="/home/phablet/Videos/VData.txt";
    file.close();
    file.setFileName(filename);
    if ( file.open(QIODevice::ReadWrite) ){
        QTextStream stream( &file );
        stream << "video "
            << now.toString() << endl;
    }else {
        qWarning() << "failed to write to" << filename << "[" << file.error() << "]";
    }
}




























QString Filewriter::filenameTransmission(QString container){
    return libertineUserDir + container + "/" + ".config/transmission/settings.json";
    //return appDataPath() + "/settings.json";
}

bool Filewriter::getTransmission(QString container){
    qDebug() << __FUNCTION__;
    QString filename = filenameTransmission(container);

    QFile file(filename);
    if (file.exists()){
        QByteArray fileData;
        if (!file.open(QIODevice::ReadWrite)){
            qCritical() << "Failed to open file" << filename << "[" << file.error() << "]";
            return -1;
        }

        QStringList propertyNames;
        QStringList propertyKeys;
        auto settings = file.readAll();
        file.close();
        QJsonDocument jsonDoc = QJsonDocument::fromJson(settings);
        qDebug() << jsonDoc.isEmpty() << jsonDoc.isObject() << jsonDoc.isArray();
        QJsonObject jsonObj = jsonDoc.object();
        qDebug()
            << "rpc-enabled" << jsonObj[QString("rpc-enabled")]
            << "watch-dir" << jsonObj["watch-dir"] // : Downloads
            << "watch-dir-enabled" << jsonObj["watch-dir-enabled"]
            << "start-added-torrents" << jsonObj["start-added-torrents"]
            << "download-dir" << jsonObj["download-dir"]
            << "show-options-window" << jsonObj["show-options-window"];
        return jsonObj["rpc-enabled"].toBool();
    } else {
        qDebug() << "File does not exist" << filename;
    }
    return false;
}

void Filewriter::setTransmission(QString container){
    qDebug() << __FUNCTION__;
    return setTransmission(container, true);
}


void Filewriter::setTransmission(QString container, bool enable ){
    qDebug() << __FUNCTION__ << enable;

    QString dirname = libertineUserDir + container + "/" + ".config/transmission/";
    QDir dir(dirname);
    if ( !dir.exists() ){
        qDebug() << "Dir not found (no such container?)" << dirname;
        return;
    }

    QString filename = filenameTransmission(container);
    QFile file(filename);
    if (file.exists()){
        qDebug() << "set";
        QByteArray fileData;
        if (!file.open(QIODevice::ReadWrite)){
            qCritical() << "Failed to open file" << filename << "[" << file.error() << "]";
            return;
        }

        QStringList propertyNames;
        QStringList propertyKeys;
        auto settings = file.readAll();
        file.close();

        QJsonDocument jsonDoc = QJsonDocument::fromJson(settings);
        qDebug() << jsonDoc.isEmpty() << jsonDoc.isObject() << jsonDoc.isArray();
        QJsonObject jsonObj = jsonDoc.object();
        jsonObj[QString("rpc-enabled")] = enable;
        jsonObj["watch-dir"] = "/home/phablet/Downloads";
        jsonObj["watch-dir-enabled"] = enable;
        jsonObj["start-added-torrents"] = enable;
        jsonObj["download-dir"] = "/home/phablet/Downloads";
        jsonObj["show-options-window"] = !enable;

        QJsonDocument newDoc(jsonObj);
        file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
        file.write(newDoc.toJson());
        file.close();

        qDebug() << "set [done]";
    } else {
        qDebug() << "File does not exist" << filename;
    }
}

void Filewriter::clearTransmission(QString container){
    return setTransmission(container, false);
}



























bool Filewriter::getXTermFont(QString container){
    qDebug() << __FUNCTION__;
    QString filename = filenameXdefaults(container);
    bool font_is_set = false;

    QFile file(filename);
    if (file.exists()){
        QByteArray fileData;
        if (!file.open(QIODevice::ReadWrite)){
            qCritical() << "Failed to open file" << filename << "[" << file.error() << "]";
            return -1;
        }

        while (!file.atEnd()){
            QString line = file.readLine();
            int index = -1;
            if (line.isEmpty()){
                continue;
            } else if (QRegExp("^ *#").indexIn(line) != -1 ){
                // comment
                continue;
            } else {
                QString face = line.replace(QRegExp("^ *xterm\\*faceName: *"), "");
                if (!face.isEmpty()){
                    qDebug() << "face=" << face;
                    font_is_set = true;
                    break;
                }

                QString size = line.replace(QRegExp("^ *xterm\\*faceSize: *"), "");
                if (!size.isEmpty()){
                    qDebug() << "size=" << size;
                    font_is_set = true;
                    break;
                }

            }
        }
        file.close();
    } else {
        qDebug() << "File does not exist" << filename;
    }
    return font_is_set;
}


void Filewriter::setXTermFont(QString container, bool clear, QString face, uint size){
    qDebug() << __FUNCTION__ << container << clear << face << size;
    bool written_face = false;
    bool written_size = false;

    QString dirname = libertineUserDir + container;
    QDir dir(dirname);
    if ( !dir.exists() ){
        qDebug() << "Dir not found (no such container?)" << dirname;
        return;
    }

    QString filename = filenameXdefaults(container);
    QFile file(filename);
    if(file.exists()){
        // change setting in file
        qDebug() << "change";
        if (!file.open(QIODevice::ReadWrite)){
            qCritical() << "Failed to open file" << filename << "[" << file.error() << "]";
            return;
        }
        QByteArray fileData;

        while (!file.atEnd()){
            QString line = file.readLine();
            if (line.isEmpty()){
                // continue;
                qDebug() << "empty";
            } else if (QRegExp("^ *#").indexIn(line) != -1 ){
                // comment
                // continue;
                qDebug() << "comment";
            } else {
                int index = line.indexOf(QRegExp("^ *xterm\\*faceName: *"));
                if (index != -1){
                    qDebug() << "before" << "[" << line << "]";
                    line.clear();
                    if (!clear){
                        QTextStream afters(&line, QIODevice::ReadWrite);
                        afters << "xterm*faceName: " << face << endl;
                        qDebug() << "after" << "[" << line << "]";
                        written_face = true;
                    }
                } else {

                    index = line.indexOf(QRegExp("^ *xterm\\*faceSize: *"));
                    if (index != -1){
                        qDebug() << "before" << "[" << line << "]";
                        line.clear();
                        if (!clear){
                            QTextStream afters(&line, QIODevice::ReadWrite);
                            afters << "xterm*faceSize: " << size << endl;
                            qDebug() << "after" << "[" << line << "]";
                            written_size = true;
                        }
                    } else {
                        qDebug() << "other [" << line << "]";
                    }
                }
            }
            fileData.append(line);
        }


        // go back to the start
        file.close();
        if (!file.open(QIODevice::ReadWrite | QIODevice::Truncate) ){
            qCritical() << "Failed to reopen file" << filename << "[" << file.error() << "]";
            return;
        }

        if (file.write(fileData) == -1){
            qCritical() << "Failed to write to file" << filename << "[" << file.error() << "]";
            return;
        }
        file.close();
        qDebug() << "change [done]";
    }

    if (!clear){
        // we should write
        if (!file.exists() || !written_face || !written_size){
            // but we haven't written yet
            qDebug() << "append";
            if ( file.open(QIODevice::ReadWrite | QIODevice::Append) ){
                QTextStream stream( &file );
                if (!written_face)
                    stream << "xterm*faceName: " << face << "\n";
                if (!written_size)
                    stream << "xterm*faceSize: " << size << "\n";
                file.close();
            }else{
                qCritical() << "Failed to append to file" << filename << "[" << file.error() << "]" ;
            }
        }
    }
    qDebug() << __FUNCTION__ << "[done]";

}

void Filewriter::setXTermFont(QString container){
    setXTermFont(container, false, "DejaVu Sans Mono", 10);
}

void Filewriter::clearXTermFont(QString container){
    setXTermFont(container, true, "", -1);
}


















QString Filewriter::filenameDotBashrc(QString container){
    return libertineUserDir + container + "/.bashrc";
}


bool Filewriter::existsDotBashrc(QString container){
    qDebug() << __FUNCTION__ ;
    QString dirname = libertineUserDir + container;
    QFile file(filenameDotBashrc(container));
    bool exists = file.exists();
    qDebug() << __FUNCTION__ << exists;
    return exists;
}

void Filewriter::writeDotBashrc(QString container){
    qDebug() << __FUNCTION__ ;
    QString dirname = libertineUserDir + container;
    QFile source("/home/phablet/.bashrc");
    QFile target(filenameDotBashrc(container));

    if( source.open(QIODevice::ReadOnly)){
        if (target.open(QIODevice::ReadWrite)){
            QTextStream out(&target);
            while (!source.atEnd()){
                out << source.readLine();
            }
            source.close();

//            out << "\n";
//            // change the prompt to:
//            // (xenial)phablet@ubuntu-phablet:~
//            // $
//            // - prefix with (LIBERTINE-CONTAINER-NAME)
//            // - username@host in brown color
//            // - current working directory in white
//            // - dollar sign on a new line
//            out << "export PS1='${debian_chroot:+($debian_chroot)}\\e[31m\\e[33m\\u@\\h\\e[0m:\\[\\033[0m\\]\\w\\[\\033[00m\\]\\n\\$ '\n";

            target.close();
        } else {
            qDebug() << "Couldn't open target";
        }
    } else {
        qDebug() << "Couldn't open source";
    }
    qDebug() << __FUNCTION__ << "[done]";

}

void Filewriter::deleteDotBashrc(QString container){
    qDebug() << __FUNCTION__ ;
    QFile bashrc(filenameDotBashrc(container));
    bashrc.remove();
}











QString Filewriter::filenameHomeDotBashrc(){
    return "/home/phablet/.bashrc";
}

static const QString lishPath("if [ -d /opt/click.ubuntu.com/libertine-tweak-tool.doniks/current/lib/arm-linux-gnueabihf/bin/ ] ; then export PATH=$PATH:/opt/click.ubuntu.com/libertine-tweak-tool.doniks/current/lib/arm-linux-gnueabihf/bin/ ; fi\n");

bool Filewriter::getLishPath(){
    qDebug() << __FUNCTION__ ;
    //qDebug() << "[" << lishPath.length() << "]" << lishPath;
    bool is_set = false;
    QFile file(filenameHomeDotBashrc());
    if ( file.open(QIODevice::ReadOnly) ){
        while(!file.atEnd()){
            QString line = file.readLine();
            if (QString(lishPath) == line ){
                is_set = true;
                qDebug() << "[FOUNDIT]"; // << line;
                break;
            } else {
                //qDebug() << "[" << line.length() << "]" << line;
            }
        }
    }
    return is_set;
}

/**
 * @brief Filewriter::setLishPath
 *
 * This rewrites the users .bashrc to extend the PATH
 * I think (tm) it works correctly, but in case something really bad happens, this is how you open a shell without bashrc:
 * adb shell /bin/bash --norc --noprofile
 */
void Filewriter::setLishPath(){
    qDebug() << __FUNCTION__ ;
    if ( getLishPath() )
        return;

    QFile file(filenameHomeDotBashrc());
    if (file.open(QIODevice::Append)){
        QTextStream out(&file);
        out << lishPath;
    }
}


void Filewriter::clearLishPath(){
    qDebug() << __FUNCTION__ ;

    if ( ! getLishPath())
        return;

    QFile file(filenameHomeDotBashrc());
    if (file.open(QIODevice::ReadOnly)){
        QByteArray newContent;
        while (!file.atEnd()){
            QString line = file.readLine();
            if (line == lishPath){
                // do not append line
                //qDebug() << "[SKIP]" << line;
            } else {
                //qDebug() << "[KEEP]" << line;
                newContent.append(line);
            }
        }
        file.close();

        if (file.open(QIODevice::ReadWrite | QIODevice::Truncate)){
            if ( file.write(newContent) == -1 ) {
                qDebug() << "Could not write";
            }
            file.close();
        } else {
            qDebug() << "Could not open" << filenameHomeDotBashrc() << "for writing";
        }
    } else {
        qDebug() << "Could not open" << filenameHomeDotBashrc() << "for reading";
    }
}










QString Filewriter::configPath()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
}
QString Filewriter::cachePath()
{
    return QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
}
QString Filewriter::appDataPath()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}
