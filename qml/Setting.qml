import QtQuick 2.4
//import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

Item {
    height: 50
    property alias title: title.text
    property alias description: description.text
    property alias checked: toggle.checked
    Column{
        anchors.left: parent.left
        anchors.right: toggle.left
        spacing: 3
        Label {
            id: title
            text:""
            font.bold: true;
            width:parent.width
        }
        Label {
            id: description
            text:""
            linkColor: "green" // the default blue looks horrible in dark mode, green looks ok in default and in dark
            wrapMode:Text.Wrap;
            width:parent.width
            onLinkActivated:{
                console.log(link)
                Qt.openUrlExternally(link)
            }
        }
    }
    Switch {
        id: toggle
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
    }
}
